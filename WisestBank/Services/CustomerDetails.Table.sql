﻿CREATE TABLE [dbo].[CustomerData]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FirstName] NVARCHAR(50) NOT NULL, 
    [LastName] NVARCHAR(50) NOT NULL, 
    [Gender] NCHAR(10) NULL, 
    [Email] NVARCHAR(50) NOT NULL, 
    [Phone] NVARCHAR(50) NOT NULL, 
    [BirthDate] DATETIME NULL, 
    [AccountNumber] NVARCHAR(25) NOT NULL, 
    [AccountBalance] MONEY NOT NULL
)
