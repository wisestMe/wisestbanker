﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WisestBank.Services
{
    class AccountOpener
    {
        public string firstName { get; set; }

        public string lastName { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string birthDateString { get; set; }


        public void GetCustomerDetails()
        {
            Console.WriteLine("Please fill in your details");
            WisestBank.Services.HorizontalRule.DrawHorizontalLine();

            Console.WriteLine("What is your first name");
            firstName = Console.ReadLine();

            Console.WriteLine("What is your last name");
            lastName = Console.ReadLine();

            Console.WriteLine("Enter Your Email Address");
            email = Console.ReadLine();

            Console.WriteLine("Enter your phone number");
            phone = Console.ReadLine();


            Console.WriteLine("Enter your date of birth (dd/mm/yr)");
            birthDateString = Console.ReadLine();

            ShowCustomerInfo(firstName, lastName, email, birthDateString);
        }

        public static void ShowCustomerInfo(string firstName, string lastName, string emailAddress, string birthDateString)
        {
            Console.WriteLine("Kindly confirm your deatail as follows");
            WisestBank.Services.HorizontalRule.DrawHorizontalLine();
            Console.WriteLine(" First Name: {0} \n Last Name: {1} \n Email: {2} \n D.O.B: {3}", firstName, lastName, emailAddress, birthDateString);

            // Generate account number
            //string accountNumber = WisestBank.Services.GenerateAccountNumber.CreateAccountNumber();

            //DataCenter.ConnectDB();
            //DataCenter.StoreData();
        }

        //public static void StoreData()
        //{
        //    AccountOpener accountOpener = new AccountOpener();
        //    //accountOpener.GetCustomerDetails();
        //    string NewAccountNumber = WisestBank.Services.GenerateAccountNumber.CreateAccountNumber();
        //    int accountBalance = 0;
        //    DataCenter.ConnectDB();

        //    DataCenter dataCenter = new DataCenter();
        //    string customerData = "INSERT INTO CustomerData (FirstName, LastName, Email, Phone, BirthDate, AccountNumber, AccountBalance) VALUES('" + accountOpener.firstName + "', " + accountOpener.lastName + ", '" + accountOpener.phone + "', " + accountOpener.email + ", '" + accountOpener.birthDateString + "', "+ NewAccountNumber +", '"+ accountBalance +"')";
        //    SqlCommand instruction = new SqlCommand(customerData, dataCenter.connect);

        //    instruction.ExecuteNonQuery();
        //    Console.WriteLine("\nData stored into database");

        //}
    }
}
