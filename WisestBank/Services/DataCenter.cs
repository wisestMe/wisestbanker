﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace WisestBank.Services
{
    class DataCenter
    {
        

        public static void ConnectDB()
        {
            SqlConnection connection;
            string connectionString;

            try
            {
                connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\wisestMe\source\repos\WisestBank\WisestBank\Services\CustomerData.mdf;Integrated Security=True";
                connection = new SqlConnection(connectionString);
                connection.Open();
                Console.WriteLine("Database connected and running");

                AccountOpener accountOpener = new AccountOpener();
                accountOpener.GetCustomerDetails();
                string NewAccountNumber = WisestBank.Services.GenerateAccountNumber.CreateAccountNumber();
                int accountBalance = 0;


                string customerData = "INSERT INTO CustomerData (FirstName, LastName) VALUES('" + accountOpener.firstName + "', " + accountOpener.lastName + ")";
                SqlCommand instruction = new SqlCommand(customerData, connection);

                instruction.ExecuteNonQuery();
                Console.WriteLine("\nData stored into database");

            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
