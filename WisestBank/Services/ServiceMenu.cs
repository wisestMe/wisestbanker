﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WisestBank.Services
{
    class ServiceMenu
    {
        public static void ListOfServices()
        {
            Console.WriteLine("Please select a number from the following list");

            string[] serviceOptions = {"Open Account", "Close Account", "Deposit", "Withdraw" };

            for (int i = 0; i < serviceOptions.Length; i++)
            {
                Console.WriteLine("{0}. {1}", i+1, serviceOptions[i]);
            }
        }

        public static string SelectedService()
        {
            string userServiceSelection = Console.ReadLine();
            return userServiceSelection;
        }
    }
}
