﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WisestBank.Services
{
    class CustomerNameAddress
    {
        public static string GetCustomerName()
        {
            Console.WriteLine("Please how would you like us to address you?");
            var customerName = Console.ReadLine();
            return customerName;
        }
    }
}
