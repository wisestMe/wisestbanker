﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WisestBank.Services
{
    class HorizontalRule
    {
        public static void DrawHorizontalLine()
        {
            var horizontalLine = "\n -------------------- \n";
            Console.WriteLine(horizontalLine);
        }
    }
}
