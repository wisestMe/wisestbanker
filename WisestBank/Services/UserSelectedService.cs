﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WisestBank.Services
{
    class UserSelectedService
    {
        public static void UserSelection()
        {
            string selectedServiceString = WisestBank.Services.ServiceMenu.SelectedService();
            

            int selectedService = int.Parse(selectedServiceString);
            Console.WriteLine(selectedService);

            switch (selectedService)
            {
                case 1:
                    Console.WriteLine("Welcome to our account opening portal");
                    //call the account opening method
                    //AccountOpener openAccount = new AccountOpener();
                    DataCenter.ConnectDB();
                    //openAccount.GetCustomerDetails();
                    break;
                case 2:
                    Console.WriteLine("We are sorry to see you go");
                    //call the account closure method
                    break;
                case 3:
                    Console.WriteLine("Please fill your deposit slip");
                    // call the account deposit method
                    break;
                case 4:
                    Console.WriteLine("Please fill your withdrawal slip");
                    // call the account withdrawal method
                    break;
                default:
                    Console.WriteLine("Invalid selection. please choose");
                    WisestBank.Services.ServiceMenu.ListOfServices();
                    break;
            }

        }
    }
}
