﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WisestBank.Services;

namespace WisestBank
{
    class Program
    {
        static void Main()
        {
            string welcomeCustomer = WelcomeMessage.GreetCustomer();
            Console.WriteLine(welcomeCustomer);
            HorizontalRule.DrawHorizontalLine();

            // Get customer name
            string preferredAddress = CustomerNameAddress.GetCustomerName();
            Console.WriteLine("Thank you {0}", preferredAddress);
            HorizontalRule.DrawHorizontalLine();

            Console.WriteLine("So {0}, how may we help you today?", preferredAddress);
            ServiceMenu.ListOfServices();
            HorizontalRule.DrawHorizontalLine();

            UserSelectedService.UserSelection();

            //AccountOpener.GetCustomerDetails();
        }
    }
}
